import { View, Text } from 'react-native';
import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';

import Dashboard from "../screens/Admin/dashboard";
import AllProduct from '../screens/Admin/Product/AllProduct';
import { BottomTabNavigator } from "./TabNavigator"

const Drawer = createDrawerNavigator();
const DrawerNavigator = () => {
  return (
    <Drawer.Navigator initialRouteName="Dashboard">
        <Drawer.Screen name="Dashboard" component={Dashboard}/>
        <Drawer.Screen name='AllProduct' component={AllProduct}/>
    </Drawer.Navigator>
  )
}

export default DrawerNavigator