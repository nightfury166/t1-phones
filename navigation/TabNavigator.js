import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { FontAwesome } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons'; 
import { MainStackNavigator, UserStackNavigator, CartStackNavigator, SettingStackNavigator } from "./StackNavigator";

const Tab = createBottomTabNavigator();

const BottomTabNavigator = () => {
  return (
    <Tab.Navigator screenOptions={{ headerShown: false, tabBarActiveTintColor: '#e91e63', }}>
      <Tab.Screen name="Index" component={MainStackNavigator} options={{ tabBarLabel: 'Home', tabBarIcon: ({ color, size }) => (<FontAwesome name="home" color={color} size={size} />) }} />
      <Tab.Screen name="Login" component={UserStackNavigator} options={{ tabBarLabel: 'User', tabBarIcon: ({ color, size }) => (<FontAwesome name="user" color={color} size={size} />) }} />
      <Tab.Screen name="cart" component={CartStackNavigator} options={{ tabBarLabel: 'Cart', tabBarIcon: ({ color, size }) => (<FontAwesome name="shopping-cart" color={color} size={size} />) }} />
      <Tab.Screen name="setting tab" component={SettingStackNavigator} options={{ tabBarLabel: 'Setting', tabBarIcon: ({ color, size }) => (<AntDesign name="setting" color={color} size={size} />) }} />
    </Tab.Navigator>
  );
};

export default BottomTabNavigator;
