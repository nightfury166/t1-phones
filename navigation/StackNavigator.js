import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { Ionicons } from '@expo/vector-icons';

import Home from "../screens/index";
import User from "../screens/User/user";
import Detail from "../screens/detailproduct";
import Login from "../screens/User/login";
import Signup from "../screens/User/signup";
import Cart from "../screens/cart";
import Setting from "../screens/setting";
import Dashboard from "../screens/Admin/dashboard";
import AllProduct from "../screens/Admin/Product/AllProduct";
import DetailProduct from '../screens/Admin/Product/DetailProduct';
import AddProduct from "../screens/Admin/Product/AddProduct";

const Stack = createStackNavigator();

const screenOptionStyle = {
  headerStyle: {
    backgroundColor: "red",
  },
  headerTintColor: "white",
  headerBackTitle: "Back",
};

const MainStackNavigator = () => {
  return (
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="Detail" component={Detail}/>
    </Stack.Navigator>
  );
}

const UserStackNavigator = () => {
  return (
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen name="User" component={User} />
      <Stack.Screen name="Login" component={Login}/>
      <Stack.Screen name="Signup" component={Signup}/>
      <Stack.Screen name="Dashboard" component={Dashboard} options={{headerLeft: null}}/>
      <Stack.Screen name="AllProduct" component={AllProduct}options={({ navigation, route }) => ({
          headerRight: () => (
            <Ionicons
              name={'ios-add-circle'}
              size={25}
              color={'white'}
              style={{ marginRight: 15 }}
              onPress={() => navigation.navigate('AddProduct')}
            />
          ),
        })}/>
      <Stack.Screen name="DetailProduct" component={DetailProduct}/>
      <Stack.Screen name="AddProduct" component={AddProduct}/>
    </Stack.Navigator>
  );
}

const CartStackNavigator = () => {
  return (
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen name="Cart" component={Cart}/>
    </Stack.Navigator>
  );
}

const SettingStackNavigator = () => {
  return (
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen name="Setting" component={Setting}/>
    </Stack.Navigator>
  )
}

export { MainStackNavigator, UserStackNavigator, CartStackNavigator, SettingStackNavigator };
