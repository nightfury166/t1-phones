import { StyleSheet, Text, View, ScrollView } from 'react-native';
import React from 'react';
import { Button, Menu, Divider, Provider } from 'react-native-paper';
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { Ionicons } from "@expo/vector-icons";
import OptionList from "../../components/OptionList/OptionList";

const dashboard = ({ navigation }) => {
    const [visible, setVisible] = React.useState(false);
    const openMenu = () => setVisible(true);
    const closeMenu = () => setVisible(false);

    return (
        <Provider>
            <View style={styles.container}>
                <Text>Chào mừng đến với trang quản trì T1-Phone</Text>
            </View>
            <View style={styles.headingContainer}>
                <MaterialCommunityIcons name="menu-right" size={30} color="black" />
                <Text style={styles.headingText}>Actions</Text>
            </View>
            <View style={{ flex: 1, width: "100%" }}>
          <ScrollView style={styles.actionContainer}>
            <OptionList
              text={"Products"}
              Icon={Ionicons}
              iconName={"md-square"}
              onPress={() =>
                navigation.navigate("AllProduct")
              }
              onPressSecondary={() =>
                navigation.navigate("AddProduct")
              }
              type="morden"
            />
            <OptionList
              text={"Categories"}
              Icon={Ionicons}
              iconName={"menu"}
              onPress={() =>
                navigation.navigate("viewcategories")
              }
              onPressSecondary={() =>
                navigation.navigate("addcategories")
              }
              type="morden"
            />
            <OptionList
              text={"Orders"}
              Icon={Ionicons}
              iconName={"cart"}
              onPress={() =>
                navigation.navigate("vieworder")
              }
              type="morden"
            />
            <OptionList
              text={"Users"}
              Icon={Ionicons}
              iconName={"person"}
              onPress={() =>
                navigation.navigate("viewusers")
              }
              type="morden"
            />

            <View style={{ height: 20 }}></View>
          </ScrollView>
        </View>
        </Provider>

    )
}

export default dashboard

const styles = StyleSheet.create({
    container: {
        marginTop: "3%",
        alignItems: "center",
    },
    btn: {
        paddingHorizontal: 10,
        paddingVertical: 5,
        backgroundColor: 'gold',
    },

    headingContainer: {
        display: "flex",
        justifyContent: "flex-start",
        paddingLeft: 10,
        width: "100%",
        alignItems: "center",
        flexDirection: "row",
    },
    actionContainer: { padding: 20, width: "100%", flex: 1 },
})