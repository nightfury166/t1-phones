import {
  View,
  Text,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  Image,
  StatusBar,
  Alert
} from 'react-native'
import React, { useState, useEffect } from 'react';
import axios from 'axios';

const AllProduct = ({ navigation }) => {
  const [product, setProduct] = useState([]);
  const [load, setLoad] = useState(false);
  const [error, setError] = useState('');
  const baseUrl = 'http://192.168.1.104';

  useEffect(() => {
    axios
      .get(`${baseUrl}:3000/products`)
      .then((res) => {
        setProduct(res.data);
        setLoad(true);
      })
      .catch((err) => {
        setError(err.message);
        setLoad(true);
      });
  }, []);

  const handleDelete = (id) => {
    setLoad(true);
    axios.delete(`${baseUrl}:3000/products`)
    .then(res => {
      this.setState({ data: res.data }).filter(item => item.id === "1");
    });
  };

  const showConfirmDialog = (id) => {
    return Alert.alert(
      "Are your sure?",
      "Are you sure you want to delete the category?",
      [
        {
          text: "Yes",
          onPress: () => {
            handleDelete(id);
          },
        },
        {
          text: "No",
        },
      ]
    );
  };

  const renderItem = ({item}) => {
    return (
      <View style={styles.listItem}>
        <Image source={{ uri: item.image }} style={{ width: 60, height: 60 }} />
        <View style={{ alignItems: "center", flex: 1 }}>
          <Text style={{ fontWeight: "bold" }}>{item.name}</Text>
          <Text style={{marginTop: "5%"}}>{item.price} VND</Text>
        </View>
        <View style={{ height: 50, width: 50, justifyContent: "center", alignItems: "center" }}>
        <TouchableOpacity onPress={() => navigation.navigate('DetailProduct',{item: item})}>
          <Text style={{ color: "green", marginBottom: 10}}>View</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={showConfirmDialog}>
          <Text style={{ color: "red", marginTop: 10}}>Delete</Text>
        </TouchableOpacity>
        </View>
      </View>
    );
  }

  return (
    <View style={styles.container}>
        <FlatList
          data={product}
          renderItem={renderItem}
          keyExtractor={(item) => item.id}
        />
      </View>
  )
}

export default AllProduct

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F7F7F7',
    marginTop: 60
  },
  listItem: {
    margin: 10,
    padding: 15,
    backgroundColor: "#FFF",
    width: "90%",
    flex: 1,
    alignSelf: "center",
    flexDirection: "row",
    borderRadius: 5
  }
})