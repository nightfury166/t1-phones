import axios from 'axios';
import React, { Component, useState, useRef } from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity } from 'react-native';
import { Picker } from '@react-native-picker/picker';


const DetailProduct = ({ route, navigation }) => {
  const { item } = route.params;
  const baseUrl = 'http://192.168.1.104';


  const [product, setProduct] = useState({
    name: item.name,
    category_id: item.category_id,
    price: item.price,
    description: item.description,
    quantity: item.quantity,
    sale: item.sale,

  });
  const onChangeName = (value) => {
    setProduct({ ...product, name: value });
  };

  const onChangeCategory_id = (value) => {
    setProduct({ ...product, category_id: value });
  };

  const onChangePrice = (value) => {
    setProduct({ ...product, price: value });
  };

  const onChangeDes = (value) => {
    setProduct({ ...product, description: value });
  };

  const onChangeQuantity = (value) => {
    setProduct({ ...product, quantity: value });
  };

  const onChangeSale = (value) => {
    setProduct({ ...product, sale: value });
  };

  const pickerRef = useRef();

  function open() {
    pickerRef.current.focus();
  }

  function close() {
    pickerRef.current.blur();
  }

  const updateData = () => {
    axios({
      method: 'put',
      url: `${baseUrl}:3000/products`,
      data: {
        name: product.name,
        category_id: product.category_id,
        price: product.price,
        description: product.description,
        quantity: product.quantity,
        sale: product.sale,
      }
    });
  };

  return (
    <View style={styles.container}>
      <View style={styles.wrapper}>
        <TextInput
          placeholder={'Name'}
          placeholderTextColor="black"
          onChangeText={(value) => onChangeName(value)}
          style={styles.input}
          value={product.name}
        />
      </View>
      <View style={styles.wrapper}>
        <Picker
          selectedValue={product.category_id}
          onValueChange={onChangeCategory_id}
          mode="dropdown">
          <Picker.Item label="Iphone" value="1" />
          <Picker.Item label="Samsung" value="2" />
        </Picker>
      </View>
      <View style={styles.wrapper}>
        <TextInput
          placeholder={'Price'}
          placeholderTextColor="black"
          onChangeText={(value) => onChangePrice(value)}
          style={styles.input}
          value={product.price}
        />
      </View>
      <View style={styles.wrapper}>
        <TextInput
          placeholder={'Description'}
          placeholderTextColor="black"
          onChangeText={(value) => onChangeDes(value)}
          style={styles.input}
          value={product.description}
        />
      </View>
      <View style={styles.wrapper}>
        <TextInput
          placeholder={'Quantity'}
          placeholderTextColor="black"
          onChangeText={(value) => onChangeQuantity(value)}
          style={styles.input}
          value={product.quantity}
        />
      </View>
      <View style={styles.wrapper}>
        <TextInput
          placeholder={'Sale'}
          placeholderTextColor="black"
          onChangeText={(value) => onChangeSale(value)}
          style={styles.input}
          value={product.sale}
        />
      </View>
      <View style={{ flexDirection: 'row' }}>
        <TouchableOpacity onPress={updateData}>
          <View style={{ backgroundColor: 'blue', padding: 10 }}>
            <Text style={{ color: 'white', textAlign: 'center' }}>Update</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  )
}

export default DetailProduct

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F7F7F7',
    marginTop: 60
  },
  input: {
    height: 40,
    borderWidth: 1,
    borderColor: '#ccc',
    padding: 10,
    marginVertical: 5,
  },
  wrapper: {
    margin: 3
  },
})