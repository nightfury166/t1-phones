import {
    Text,
    View,
    StyleSheet,
    TextInput,
    TouchableOpacity,
    Image,
    Button 
} from 'react-native'
import React, { useState, useRef } from 'react'
import axios from 'axios';
import { Picker } from '@react-native-picker/picker';

const AddProduct = () => {
    const baseUrl = 'http://192.168.1.104';

    const [name, setName] = useState("");
    const [category_id, setCatgory_id] = useState("");
    const [price, setPrice] = useState("");
    const [description, setDescription] = useState("");
    const [quantity, setQuantity] = useState("");
    const [sale, setSale] = useState("");
    const [image, setImage] = useState("");
    const [isLoading, setIsLoading] = useState(false);

    const onChangeName = (name) => {
        setName(name);
    };
    const onChangeCategory_id = (category_id) => {
        setCatgory_id(category_id);
    };
    const onChangePrice = (price) => {
        setPrice(price);
    };
    const onChangeDes = (description) => {
        setDescription(description);
    };
    const onChangeQuantity = (quantity) => {
        setQuantity(quantity);
    };
    const onChangeSale = (sale) => {
        setSale(sale);
    };
    const onChangeImage = (image) => {
        setImage(image);
    };
    const pickerRef = useRef();

    function open() {
        pickerRef.current.focus();
    }

    function close() {
        pickerRef.current.blur();
    }
    const onSubmitFormHandler = async (event) => {
        setIsLoading(true);
        try {
            const response = await axios.post(`${baseUrl}:3000/products`, {
                name,
                category_id,
                price,
                description,
                quantity,
                sale,
                image,
            });
            if (response.status === 201) {
                alert(` You have created: ${JSON.stringify(response.data)}`);
                setIsLoading(false);
                setName('');
                setCatgory_id('');
                setPrice('');
                setDescription('');
                setQuantity('');
                setSale('');
                setImage('');
            } else {
                throw new Error("An error has occurred");
            }
        } catch (error) {
            alert("An error has occurred");
            setIsLoading(false);
        }
    };

    return (
        <View style={styles.container}>
            <View style={styles.wrapper}>
                {isLoading ? (
                    <Text style={styles.formHeading}> Creating resource </Text>
                ) : (
                    <Text style={styles.formHeading}>Detail</Text>
                )}
            </View>
            <View style={styles.wrapper}>
                <TextInput
                    placeholder="Name"
                    placeholderTextColor="black"
                    style={styles.input}
                    value={name}
                    editable={!isLoading}
                    onChangeText={onChangeName}
                />
            </View>
            <View style={styles.wrapper}>
                <Picker
                    selectedValue={category_id}
                    onValueChange={onChangeCategory_id}
                    mode="dropdown">
                    <Picker.Item label="Iphone" value={category_id}/>
                </Picker>
            </View>
            <View style={styles.wrapper}>
                <TextInput
                    placeholder="Price"
                    placeholderTextColor="black"
                    style={styles.input}
                    value={price}
                    editable={!isLoading}
                    onChangeText={onChangePrice}
                />
            </View>
            <View style={styles.wrapper}>
                <TextInput
                    placeholder="Description"
                    placeholderTextColor="black"
                    style={styles.input}
                    value={description}
                    editable={!isLoading}
                    onChangeText={onChangeDes}
                />
            </View>
            <View style={styles.wrapper}>
                <TextInput
                    placeholder="Quantity"
                    placeholderTextColor="black"
                    style={styles.input}
                    value={quantity}
                    editable={!isLoading}
                    onChangeText={onChangeQuantity}
                />
            </View>
            <View style={styles.wrapper}>
                <TextInput
                    placeholder="Sale"
                    placeholderTextColor="black"
                    style={styles.input}
                    value={sale}
                    editable={!isLoading}
                    onChangeText={onChangeSale}
                />
            </View>
            <View>
                <Button
                    title="Submit"
                    onPress={onSubmitFormHandler}
                    style={styles.submitButton}
                    disabled={isLoading}
                />
            </View>
        </View>
    )
}

export default AddProduct

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 10,
        padding: 8,
        margin: 15,
    },
    input: {
        height: 40,
        borderWidth: 1,
        borderColor: '#ccc',
        padding: 10,
        marginVertical: 5,
    },
    wrapper: {
        margin: 3
    },
})