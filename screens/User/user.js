import { View, Text, Image, StyleSheet, TouchableOpacity, SafeAreaView } from 'react-native'
import React from 'react'

export default function App({ navigation }) {
  return (
    <SafeAreaView style={styles.container}>
      <View style={{ alignItems: "center", width: "90%" }}>
        <Image
          source={require("../../assets/logo.png")}
          style={{ height: 120, width: 120, marginVertical: 7 }}
        />
        <Text
          style={{
            color: "rgb(86,12,206)",
            fontSize: 20,
            fontWeight: "600",
            marginVertical: 7,
          }}
        >
          Login Template
        </Text>
        <Text
          style={{
            fontSize: 18,
            textAlign: "center",
            marginVertical: 7,
            width: "100%",
          }}
        >
          The easiest way to start with your amazing application
        </Text>
        <TouchableOpacity onPress={() => navigation.navigate('Login')}
          style={{
            backgroundColor: "rgb(86,12,206)",
            width: "100%",
            padding: 15,
            borderRadius: 10,
            marginVertical: 7,
          }}
        >
          <Text
            style={{
              color: "white",
              textAlign: "center",
              fontWeight: "500",
              fontSize: 18,
            }}
          >
            LOGIN
          </Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('Signup')}
          style={{
            width: "100%",
            padding: 15,
            borderWidth: 1,
            borderColor: "#f5f5f5",
            borderRadius: 10,
            marginVertical: 7,
          }}
        >
          <Text
            style={{
              color: "rgb(86,12,206)",
              textAlign: "center",
              fontWeight: "500",
              fontSize: 18,
            }}
          >
            SIGN UP
          </Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
