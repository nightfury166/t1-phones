import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity } from 'react-native';
import React from 'react'

export default function App({ navigation }) {
  return (
    <View style={styles.container}>
      <View style={{
        width: "90%",
        alignItems: 'center'
      }}>
        <Image
          source={require("../../assets/logo.png")}
          style={{
            width: 100,
            height: 100,
          }}>
        </Image>
        <Text style={{
          textAlign: 'center',
          color: 'rgb(86,12,206)',
          fontSize: 17,
          fontWeight: 'bold',
          margin: 10
        }}>
          Welcome back.
        </Text>
        <TextInput
          placeholder="Email"
          style={styles.input}
        />
        <TextInput
          placeholder="Password"
          style={styles.input}
        />
        <Text style={{
          width: '100%',
          textAlign: 'right',
          fontSize: 14,
          color: '#9c9c9c',
          marginBottom: 20

        }}>
          Forgot your password?
        </Text>
        <View style={styles.button_login}>
          <TouchableOpacity onPress={() => navigation.navigate("Dashboard")}>
            <Text style={{
              textAlign: 'center',
              color: 'white',
              lineHeight: 50,
              fontWeight: "500"
            }}>
              LOGIN
            </Text>
          </TouchableOpacity>

        </View>
        <Text>
          Don't have an account ?
          <TouchableOpacity onPress={() => navigation.navigate("Signup")}>
            <Text style={{
              fontWeight: 'bold',
              color: 'rgb(86,12,206)'
            }}> Sign up</Text>
          </TouchableOpacity>
        </Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',

  },
  input: {
    width: "100%",
    padding: 15,
    borderColor: '#bfbfbf',
    borderWidth: 1,
    borderRadius: 5,
    margin: 10,
    height: 50,
    color: '#bfbfbf'
  },

  button_login: {
    width: "100%",
    height: 50,
    backgroundColor: "rgb(86,12,206)",
    borderRadius: 5,
    margin: 15
  }
});