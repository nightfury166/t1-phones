import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity } from 'react-native';

export default function App({ navigation }) {
    return (
        <View style={styles.container}>
            <View style={{
                width: "90%",
                alignItems: 'center'
            }}>
                <Image
                    source={require("../../assets/logo.png")}
                    style={{
                        width: 100,
                        height: 100,
                    }}>
                </Image>
                <Text style={{
                    textAlign: 'center',
                    color: 'rgb(86,12,206)',
                    fontSize: 17,
                    fontWeight: 'bold',
                    margin: 10
                }}>
                    Create Account
                </Text>
                <TextInput
                    placeholder="Name"
                    style={styles.input}
                />
                <TextInput
                    placeholder="Email"
                    style={styles.input}
                />
                <TextInput
                    placeholder="Password"
                    style={styles.input}
                />
                <View style={styles.button_signup}>
                    <Text style={{
                        textAlign: 'center',
                        color: 'white',
                        lineHeight: 50,
                        fontWeight: "500"
                    }}>
                        SIGN UP
                    </Text>
                </View>
                <Text>
                    Alread have an account ?
                    <TouchableOpacity onPress={() => navigation.navigate("Login")}>
                        <Text style={{
                            fontWeight: 'bold',
                            color: 'rgb(86,12,206)'
                        }}> Login</Text>
                    </TouchableOpacity>
                </Text>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',

    },
    input: {
        width: "100%",
        padding: 15,
        borderColor: '#bfbfbf',
        borderWidth: 1,
        borderRadius: 5,
        margin: 10,
        height: 50,
        color: '#bfbfbf'
    },

    button_signup: {
        width: "100%",
        height: 50,
        backgroundColor: "rgb(86,12,206)",
        borderRadius: 5,
        margin: 15
    }
});
