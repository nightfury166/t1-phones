import React from 'react';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet ,Text, View, Image, TouchableOpacity, ScrollView, WebView } from 'react-native';

export default function App({ route, navigation }) {
  const { item } = route.params;
  return (
    <ScrollView style={styles.container}>
      <View style={styles.viewimg}>
        <Image
        style={styles.product_img}
        source={{uri: item.image}}
      />
      </View>
      <Text style={styles.product_name}>{(item.name)}</Text>

      <Text style={styles.product_price}>{(item.price)} Vnđ</Text>

      <Text style={styles.title}>
        Phiên Bản:
      </Text>
      <View>
        <View style={styles.fixToText}>
          <TouchableOpacity style={styles.btn}>
            <Text style={styles.textitem}>128</Text>
          </TouchableOpacity>
            
          <TouchableOpacity style={styles.btn}>
            <Text style={styles.textitem}>256</Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.btn}>
            <Text style={styles.textitem}>512</Text>
          </TouchableOpacity>
            
        </View>
      </View>

      <Text style={styles.title}>
        Màu sắc:
      </Text>

      <View>
        <View style={styles.colorp}>
          <TouchableOpacity style={styles.goldp}>
            <Text style={styles.textitem}>Gold</Text>
          </TouchableOpacity>
            
          <TouchableOpacity style={styles.silverp}>
            <Text style={styles.textitem}>Bạc</Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.blackp}>
            <Text style={styles.textitem}>Đen</Text>
          </TouchableOpacity>
            
        </View>
      </View>

    <TouchableOpacity style={styles.addtocart}>
          <Text style={styles.textitem}>Thêm vào giỏi hàng</Text>
        </TouchableOpacity>

        <Text style={styles.product_description}>
      Mô tả sản phẩm :</Text>

      <Text style={styles.product_des}>
      {(item.description)}</Text>
      <StatusBar style="auto" />
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  btn: {
    backgroundColor: '#FC8484',
    padding:20,
    width:100,
    alignItems:'center',
    borderRadius: 15,
    borderWidth: 1.2,
    borderColor: 'gray',
  },

  goldp: {
    backgroundColor: '#edb424',
    padding:20,
    width:100,
    alignItems:'center',
    borderRadius: 15,
    borderWidth: 1.2,
    borderColor: 'gray',
  },

  silverp: {
    backgroundColor: 'silver',
    padding:20,
    width:100,
    alignItems:'center',
    borderRadius: 15,
    borderWidth: 1.2,
    borderColor: 'gray',
  },

  blackp: {
    backgroundColor: 'black',
    padding:20,
    width:100,
    alignItems:'center',
    borderRadius: 15,
    borderWidth: 1.2,
    borderColor: 'gray',
  },

  textitem: {
    fontWeight: 'bold',
    fontSize: 17,
    color: '#fff',
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    // alignItems: 'center',
  },

  product_img: {
    flex: 1,
    position: 'absolute',
    width: 300,
    height: 300,
    marginHorizontal: '15%',
    marginVertical: 50
  },

  product_name: {
    marginLeft: 1,
    textAlign: 'center',
    marginVertical: 15,
    color: 'red',
    fontSize: 22,
    fontWeight: '500',
  },

  product_price: {
    textAlign: 'center',
    fontSize: 22,
    color: 'red',
    fontWeight: '500',
  },

  fixToText: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    marginHorizontal: '10%',

  },
  viewimg: {
    backgroundColor: '#fff',
    width: '100%',
    height: 400,
    borderWidth: 2,
    borderColor: '#E7E6E6',
  },
  title: {
    textAlign: 'left',
    marginVertical: 15,
    marginLeft: 15,
    fontSize: 23,
  },

  product_des: {
    width: '60%',
    marginVertical: 5,
    marginHorizontal: '10%',
    fontWeight: '500',
    fontSize: 18,
  },

  product_description: {
    textAlign: 'left',
    marginVertical: 15,
    marginLeft: 15,
    fontSize: 23,
  },

  addtocart: {
    backgroundColor: 'red',
    padding: 10,
    marginVertical: 10,
    marginHorizontal: '10%',
    alignItems: 'center',
    borderRadius: 15,
    width: '80%',
  },

  colorp: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    marginHorizontal: '10%',
    marginVertical: 10,
  },
});