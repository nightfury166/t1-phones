import { StyleSheet, Text, View } from 'react-native'
import React from 'react'

const setting = () => {
    return (
        <View style={styles.container}>
            <Text>Hệ thống đang được bảo trì</Text>
        </View>
    )
}

export default setting

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",
        alignItems: "center",
        justifyContent: "center",
    },
})