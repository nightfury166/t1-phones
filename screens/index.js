import React, { useState, useEffect } from 'react'
import {
  Text,
  View,
  StyleSheet,
  Searchbar,
  TextInput,
  Image,
  ScrollView,
  Item,
  FlatList,
  TouchableOpacity,
  StatusBar
} from 'react-native';
// import Constants from 'expo-constants';
import GridFlatList from 'grid-flatlist-react-native';
import axios from 'axios'

export default function App({ navigation }) {
  const [data, setData] = useState([]);
  const [isLoading, setLoading] = useState(true);
  const baseUrl = 'http://192.168.1.104';

  useEffect(() => {
    axios.get(`${baseUrl}:3000/products`)
      .then((json) => setData(json.data))
      .finally(() => setLoading(false));
  });

  const renderItem = (item) => {
    return (
      <TouchableOpacity style={styles.product} 
      onPress={() => {
        navigation.navigate('Detail', {
          item: item
        });
      }}>
        <Image style={styles.product_img} source={{ uri: item.image }} />
        <Text style={styles.product_name}>{item.name}</Text>
        <Text style={styles.product_price}>{item.price} VND</Text>
      </TouchableOpacity>
    );
  }
  return (
    <View style={styles.container}>
      <View
        style={{
          backgroundColor: 'red',
          height: 60,
        }}>
        <TextInput placeholder="Tìm kiếm trên T1-Phones" style={styles.input} />
      </View>

        <View>
          <Image
            style={styles.banner}
            source={{
              uri: 'https://clickbuy.cdn.vccloud.vn/uploads/2020/12/Xs-banner.jpg',
            }}
          />
        </View>
        <View style={{ flex: 1 }}>
          <Text style={styles.title}>Điện thoại Iphone</Text>
          <View styles={{ flexDirection: 'row', flexWrap: 'wrap' }}>
            <GridFlatList
              data={data}
              renderItem={renderItem}
              keyExtractor={(item) => item.id}
            />
            <StatusBar />
          </View>
        </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // paddingTop: Constants.statusBarHeight,
    justifyContent: 'center',
    // padding: 8,
    backgroundColor: 'white',
  },
  input: {
    padding: 10,
    borderColor: 'black',
    borderWidth: 1,
    color: 'black',
    borderRadius: 5,
    backgroundColor: 'white',
    marginVertical: 10,
    marginHorizontal: 4,
  },
  banner: {
    width: '100%',
    height: 150,
  },
  title: {
    marginLeft: "5%",
    marginVertical: "3%",
    fontWeight: 'bold',
    fontSize: 18,
    color: 'red'
  },
  product: {
    // backgroundColor: 'white',
    height: 200,
    borderWidth: 2,
    borderStyle: 'solid',
    borderColor: '#FB8181',
    borderRadius: 7,
    marginHorizontal: '3%'
  },
  product_img: {
    width: 130,
    height: 130,
    // backgroundColor: 'red',
    marginHorizontal: 25,
    marginTop: 5,
    // marginLeft: "15%"
  },
  product_name: {
    marginVertical: 1,
    fontWeight: 'bold',
    marginLeft: 2,
  },
  product_price: {
    textAlign: 'center',
    marginTop: 4,
    color: 'red',
    fontWeight: 'bold',
  },
});
