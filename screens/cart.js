import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  ScrollView,
  Image,
} from "react-native";
import React, { useState } from "react";
import { Ionicons, AntDesign } from "@expo/vector-icons";
import detailprodcut from "./detailproduct";
const { width } = Dimensions.get("window");

const App = () => {
  const [count, setCount] = useState(1);
  const onAdd = () => setCount(prevCount => prevCount + 1);
  const onSub = () => setCount(prevCount => prevCount - 1);
  const onRemove = () => {
    setTodos(todos.filter(todo => todo.id !== id));
  };

  return (
    <View style={{ flex: 1, alignItems: "center", justyfyContent: "center" }}>
      <View style={{ height: 20 }} />

      <Text
        style={{
          fontSize: 28,
          color: "gray",
          fontWeight: "bold",
          textAlign: "center",
          marginBottom: 15,
        }}>
        Cart Phones
      </Text>

      <View style={{ height: 10 }} />

      <View style={{ backgroundColor: "transparent", flex: 1 }}>
        <ScrollView>
          <TouchableOpacity>
            <View
              style={{
                width: width - 20,
                flexDirection: "row",
                borderBottomWidth: 2,
                borderColor: "#cccccc",
                paddingBottom: 10,
              }}>
              <Image
                style={{
                  width: width / 3,
                  height: width / 3,
                  marginRight: 5,
                }}
                source={{
                  uri: "https://image.thanhnien.vn/w2048/Uploaded/2022/aybunux/2022_08_04/1-9005.jpg",
                }}
              />

              <View
                style={{
                  backgroundColor: "transparent",
                  flex: 1,
                  justifyContent: "space-between",
                }}>
                <View
                  style={{
                    backgroundColor: "transparent",
                    flexDirection: "row",
                    justifyContent: "space-between",
                  }}>
                  <View>
                    <Text style={{ fontSize: 20, fontWeight: "bold" }}>
                      Iphone 14
                    </Text>

                    <Text>Phân loại: red</Text>
                  </View>

                  <TouchableOpacity
                    style={{
                      borderColor: "transparent",
                      backgroundColor: "transparent",
                      marginRight: 15,
                    }}>
                    <AntDesign name="delete" size={20} color="red" />
                  </TouchableOpacity>
                </View>

                <View
                  style={{
                    backgroundColor: "transparent",
                    flexDirection: "row",
                    justifyContent: "space-between",
                  }}>
                  <Text
                    style={{
                      fontWeight: "bold",
                      color: "pink",
                      fontSize: 20,
                    }}>
                    $780
                  </Text>
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <TouchableOpacity style={styles.button} onPress={onSub}>
                      <Ionicons name="remove-circle" size={24} color="black" />
                    </TouchableOpacity>
                    <Text style={{ fontWeight: "bold", paddingHorizontal: 8 }}>
                      {count}
                    </Text>

                    <TouchableOpacity style={styles.button} onPress={onAdd}>
                      <Ionicons name="add-circle" size={24} color="black" />
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </View>

            <View style={{ height: 20 }} />
          </TouchableOpacity>
          <TouchableOpacity>
            <View
              style={{
                width: width - 20,
                flexDirection: "row",
                borderBottomWidth: 2,
                borderColor: "#cccccc",
                paddingBottom: 10,
              }}>
              <Image
                style={{
                  width: width / 3,
                  height: width / 3,
                  marginRight: 5,
                }}
                source={{
                  uri: "https://image.thanhnien.vn/w2048/Uploaded/2022/aybunux/2022_08_04/1-9005.jpg",
                }}
              />

              <View
                style={{
                  backgroundColor: "transparent",
                  flex: 1,
                  justifyContent: "space-between",
                }}>
                <View
                  style={{
                    backgroundColor: "transparent",
                    flexDirection: "row",
                    justifyContent: "space-between",
                  }}>
                  <View>
                    <Text style={{ fontSize: 20, fontWeight: "bold" }}>
                      Iphone 14
                    </Text>

                    <Text>Phân loại: red</Text>
                  </View>

                  <TouchableOpacity
                    style={{
                      borderColor: "transparent",
                      backgroundColor: "transparent",
                      marginRight: 15,
                    }}>
                    <AntDesign
                      name="delete"
                      size={20}
                      color="red"
                      onRemove={onRemove}
                    />
                  </TouchableOpacity>
                </View>

                <View
                  style={{
                    backgroundColor: "transparent",
                    flexDirection: "row",
                    justifyContent: "space-between",
                  }}>
                  <Text
                    style={{
                      fontWeight: "bold",
                      color: "pink",
                      fontSize: 20,
                    }}>
                    $780
                  </Text>
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <TouchableOpacity style={styles.button} onPress={onSub}>
                      <Ionicons name="remove-circle" size={24} color="black" />
                    </TouchableOpacity>
                    <Text style={{ fontWeight: "bold", paddingHorizontal: 8 }}>
                      {count}
                    </Text>

                    <TouchableOpacity style={styles.button} onPress={onAdd}>
                      <Ionicons name="add-circle" size={24} color="black" />
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </View>

            <View style={{ height: 20 }} />
          </TouchableOpacity>
        </ScrollView>
      </View>
      <TouchableOpacity
        style={{
          backgroundColor: "red",
          width: width - 40,
          alignItems: "center",
          padding: 10,
          borderRadius: 5,
        }}>
        <Text
          style={{
            fontSize: 24,
            fontWeight: "bold",
            color: "white",
          }}>
          CHECK OUT
        </Text>
      </TouchableOpacity>

      <View style={{ height: 10 }} />
    </View>
  );
};

const styles = StyleSheet.create({});

export default App;
